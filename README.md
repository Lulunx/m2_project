# M2_Project

Initialise database
---
* Get the script in /database/script.sql
* Exec the script on your own database
* Configure your /server/settings.json file,
Exemple
```
{
  "db" : {
    "database" : "chess",
    "username" : "chess",
    "password" : "myPassword",
    "host" : "127.0.0.1"
  },
  "express" : {
    "hostname" : "localhost",
    "port" : 8085
  },
  "secret": "mySecretForBCrypt"
}
```

Launch client
---
```
cd client
npm install
npm run serve
```
*   Configure if needed the server address in /client/src/store/index.js -> apiAddress
*   Configure if needed the socket address in /client/src/App.vue -> line 60/70


Lauch server
---
```
cd server
npm install
node src/main/server.js
OR
npm start                   #if nodemon installed