const userLocalStorage = JSON.parse(localStorage.getItem('user'));
const initialState = {
    status: {
        loggedIn: !!userLocalStorage,
        online: false
    },
    user: userLocalStorage
};

export const user = {
    namespaced: true,
    state: initialState,
    actions: {
        login: ({ rootState, commit }, data) => {
            fetch(rootState.apiAddress + "users/authenticate",
                {
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    method: "POST",
                    body: JSON.stringify(data)
                })
                .then(res => res.json()
                    .then(res => {
                        if (res.status >= 400) {
                            commit("snack/updateMessage", res.message, { root: true });
                            commit("snack/setColor", "error", { root: true });
                            commit("snack/show", true, { root: true });
                        } else {
                            commit("snack/updateMessage", res.message, { root: true });
                            commit("snack/setColor", "success", { root: true });
                            commit("snack/show", true, { root: true });
                            commit("login", res.content);
                        }
                    }));
        },
        logout: ({ commit }) => {
            commit("logout");
            commit("room/leave", null, { root: true });
        }
    },
    mutations: {
        login: (state, user) => {
            state.status.loggedIn = true;
            state.user = user;
            localStorage.setItem('user', JSON.stringify(user));
        },
        logout: (state) => {
            state.status.loggedIn = false;
            state.user = null;
            state.status.online = false;
            localStorage.removeItem('user');

        },
        userGoOnline: (state) => {
            state.status.online = true;
        },
        userGoOffline: (state) => {
            state.status.online = false;
        }
    }
};
