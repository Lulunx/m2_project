import Vue from 'vue'
import Vuex from 'vuex'

import {snack} from './snack.module';
import {user} from "./user.module";
import {room} from "./room.module";
import Settings from "../../../server/settings";

Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		snack,
		user,
		room
	},
	state: {
		apiAddress: `http://localhost:${Settings.express.port}/`,
		//apiAddress: 'http://185.229.179.183:8078/'
	},
})
