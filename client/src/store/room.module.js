import FenParser from "@chess-fu/fen-parser";


export const room = {
    namespaced: true,
    state: {
        id: -1,
        in: false,
        host: {
            id: "",
            name: "",
            color: "w",
            timeLeft: { time: 900, timer: null }
        },
        guest: {
            id: "",
            name: "",
            color: "b",
            ready: false,
            timeLeft: { time: 900, timer: null }
        },
        game: {
            started: false,
            board: ["rnbqkbnr", "pppppppp", "--------", "--------", "--------", "--------", "PPPPPPPP", "RNBQKBNR"],
            turn: "",
            fen: null,
            //NEW
            isCheck: false,
            lastMove: null,
            bestMove: null,
            showBestMove: false
        }
    },
    actions: {
        create: ({ commit }, data) => {
            commit("enter", data);
            let fen = new FenParser(data.fen);
            commit("newBoard", fen);
        },
        join: ({ commit }, data) => {
            commit("enter", data);
            let fen = new FenParser(data.fen);
            commit("newBoard", fen);
            commit("guestArrival", data);
        },
        gameUpdate: ({ commit }, data) => {
            let fen = new FenParser(data.fen);
            commit("newBoard", fen);
            //if(this.game.started) {
            commit("whoSTurn", { turn: fen.turn, timers: data.timers });
            commit("updateBestMove", data.bestMove);
            commit("updateLastMove", data.lastMove);
            commit("updateInCheck", data.incheck);
            //}
        },
        gameStarting: ({ state }) => {
            state.game.started = true;
        },
        leave: ({ commit }) => {
            commit('leave');
        },
        stopTimers : ({commit}) => {
            commit('stopTimers');
        }

    },
    mutations: {
        newBoard: (state, data) => {
            state.game.board = data.ranks;
            state.game.fen = data.toString();
        },
        updateBestMove: (state, data) => {
            state.game.bestMove = data;
        },
        updateLastMove: (state, data) => {
            state.game.lastMove = data;
        },
        updateInCheck: (state, data) => {
            state.game.isCheck = data;
        },
        whoSTurn: (state, data) => {
            state.host.timeLeft.time = data.timers.host;
            state.guest.timeLeft.time = data.timers.guest;
            if (state.game.turn !== data.turn) {
                if (data.turn === "b") {
                    state.guest.timeLeft.timer = setInterval(() => {
                        state.guest.timeLeft.time -= 1;
                    }, 1000);
                    clearInterval(state.host.timeLeft.timer);
                } else {
                    state.host.timeLeft.timer = setInterval(() => {
                        state.host.timeLeft.time -= 1;
                    }, 1000);
                    clearInterval(state.guest.timeLeft.timer);
                }
                state.game.turn = data.turn;
            }
        },
        enter: (state, data) => {
            // Room
            state.in = true;
            state.id = data.roomid;

            // Host
            state.host.name = data.host.name;
            state.host.id = data.host.id;

            // Game
            state.guest.ready = false;

            // Guest
            state.game.started = false;
            state.game.fen = data.fen;
            state.game.isCheck = false;
            state.game.bestMove = null;
            state.game.lastMove = null;
        },
        guestArrival: (state, data) => {
            // Guest
            state.guest.name = data.guest.name;
            state.guest.id = data.guest.id;
            state.guest.ready = false;
        },
        leave: (state) => {
            // Room
            state.in = false;
            state.id = -1;

            // Host
            state.host.name = "";
            state.host.id = "";
            state.host.timeLeft.time = 900;

            // Guest
            state.guest.name = "";
            state.guest.id = "";
            state.guest.ready = false;
            state.guest.timeLeft.time = 900;

            // Game
            state.game.started = false;
            state.game.fen = "";

            clearInterval(state.host.timeLeft.timer);
            clearInterval(state.guest.timeLeft.timer);

        },
        guestReady: (state) => {
            state.guest.ready = true;
        },
        showBestMove: (state) => {
            state.game.showBestMove = true;
        },
        hideBestMove: (state) => {
            state.game.showBestMove = false;
        },
        stopTimers: (state) => {
            clearInterval(state.host.timeLeft.timer);
            clearInterval(state.guest.timeLeft.timer);
        }
    }
};
