export const snack = {
    namespaced: true,
    state: {
        message: '',
        show: false,
        color: "success",
    },
    mutations: {
        updateMessage: (state, msg) => {
            state.message = msg;
        },

        show: (state, status) => {
            state.show = status;
        },

        setColor: (state, color) => {
            state.color = color;
        },
    }
};
