import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import moment from "moment";


Vue.config.productionTip = false;


Vue.filter("date", function (value) {
  if (moment(value).isValid()) {
      return moment(String(value)).format("YYYY-MM-DD");
  } else return value;
});

Vue.filter("minutes", function (value) {
  let minutes = Math.floor(value / 60);
  let secondes = value - 60*minutes;
  if(secondes < 10 )
  {
    secondes = "0"+secondes;
  }
  return minutes+":"+secondes;

});


new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app');
