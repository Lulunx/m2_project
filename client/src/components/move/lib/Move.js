
export default {
    name: "Move",
    components: {
        
    },
    data() {
        return {
            "a":0
        }
    },
    props:{
        x: Number,
        y: Number,
    },
    sockets: {

    },
    methods: {
        clickMove(){
            this.$emit('moveClick', {x:this.x, y:this.y});
        }
    },
    computed : {
		
	}
}