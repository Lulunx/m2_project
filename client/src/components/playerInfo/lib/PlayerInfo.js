
export default {
    name: "Move",
    components: {

    },
    data() {
        return {

        }
    },
    props: {
        player: {
            type: Object,
            default: function () {
                return {}
            }
        }
    },
    sockets: {

    },
    methods: {

    },
    computed: {
        timeLeft: function () {
            return this.player.timeLeft;
        },
        isItMyTurn() {
            let currentTurn = this.$store.state.room.game.turn;
            if (this.isGuest) {
                return currentTurn === this.$store.state.room.guest.color;
            }
            return currentTurn === this.$store.state.room.host.color;
        }

    },
    watch: {
        timeLeft: function (newVal) {
            if (this.isItMyTurn) {
                if (newVal == 0) {
                    this.$socket.emit("chess", {
                        type: "timeout",
                    });
                }
            }
        }
    }
}