import ConfirmDialog from "../../ConfirmDialog/ConfirmDialog";

export default {
    name: "Move",
    components: {
        ConfirmDialog
    },
    data() {
        return {
            dialog: false,
        }
    },
    props: {

    },
    sockets: {
        playerHasLeft: function () {
            this.$store.commit("room/leave");
        },
        guestIsReady: function () {
            this.$store.commit("room/guestReady");
        }
    },
    methods: {
        leave: function () {
            this.$store.commit('room/leave');
            this.$socket.emit("leaveRoom");
        },
        forfeit: function () {
            this.$socket.emit("chess", {
                type: "forfeit",
            });
            this.leave();
        },
        guestPressReady: function () {
            this.$socket.emit("readyToPlay");
        },
        hostPressStart: function () {
            if (this.$store.state.room.guest.ready) {
                this.$socket.emit("startGame", { playerTime: this.$store.state.room.host.timeLeft.time });
            } else {
                this.$store.commit("snack/updateMessage", "The Guest is not ready");
                this.$store.commit("snack/setColor", "warning");
                this.$store.commit("snack/show", true);
            }
        },
        onCopy: function () {
            this.$store.commit("snack/updateMessage", "Current FEN copied to clipboard !");
            this.$store.commit("snack/setColor", "success");
            this.$store.commit("snack/show", true);
        },
        onError: function () {
            this.$store.commit("snack/updateMessage", "An error occured while copying FEN to clipboard !");
            this.$store.commit("snack/setColor", "error");
            this.$store.commit("snack/show", true);
        },

        showBestMove: function () {
            this.$store.commit("room/showBestMove");
        },
        hideBestMove: function () {
            this.$store.commit("room/hideBestMove");
        }
    },
    computed: {
        isGuest() {

            return this.$store.state.room.guest.id === this.$store.state.user.user.user.userid;
        },
        roomID() {
            return this.$store.state.room.id;
        },
        hostPlayerName() {
            return this.$store.state.room.host.name;
        },
        hostId() {
            return this.$store.state.room.host.id;
        },
        guestReady() {
            return this.$store.state.room.guest.ready ? "Ready" : "Not Ready";
        },
        gameStarted() {
            return this.$store.state.room.game.started ? "Started" : "Not Started";
        },
        currentFEN() {
            return this.$store.state.room.game.fen;
        },
        isGameStarted() {
            return this.$store.state.room.game.started;
        },
        isGuestReady() {
            return this.$store.state.room.guest.ready;
        },
        isShowingBestMove() {
            return this.$store.state.room.game.showBestMove;
        }
    }
}