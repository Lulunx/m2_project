import {extend, ValidationObserver, ValidationProvider} from 'vee-validate';
import {required} from 'vee-validate/dist/rules';

extend('required', {
    ...required,
    message: 'This field is required'
});

export default {
    name: "signinform",
    components: {
        ValidationProvider,
        ValidationObserver
    },
    data() {
        return {
            "show": false,
            "valid": false,
            "data": {
                "pseudo": "",
                "password": "",
            },
        }
    },
    computed: {
        storedUser() {
            return this.$store.state.user.user;
        }
    },
    watch: {
        storedUser() {
            this.$router.push('/');
        }
    },
    methods: {
        onSubmit() {
            this.$store.dispatch('user/login', this.data);
        }
    }
}