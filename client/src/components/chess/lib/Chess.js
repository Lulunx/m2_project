import Cell from "../../cell/Cell";
import Move from "../../move/Move";
import backgroundInfoChess from "../../backgroundInfoChess/backgroundInfoChess";
import PromotionModal from "../../promotionModal/PromotionModal";
import ConfirmDialog from "../../ConfirmDialog/ConfirmDialog";

export default {
    name: "Chess",
    components: {
        Cell,
        Move,
        PromotionModal,
        backgroundInfoChess,
        ConfirmDialog
    },
    data() {
        return {
            "a": 0,
            pieces: [],
            moves: [],
            pieceSelected: { x: -1, y: -1 },
            modal: {
                show: false,
                promotions: [],
                move: ""
            },
            showEndDialog: false,
            endDialogMsg: ""
        }
    },
    sockets: {
        newBoard: function (data) {
            this.$store.dispatch("room/gameUpdate", data);
        },
        authorizedPosition: function (data) {
            this.moves = data;
            if (this.moves.length === 0) {
                this.unSelectPiece();
            }
        },
        endGame: function (data) {
            let win = false;
            this.$socket.emit("chess", { type: "leaveRoom" })
            this.$store.dispatch("room/stopTimers");

            if (data.res == "draw") {
                this.endDialogMsg = "The game is over, it's a draw because : " + data.why;
                this.showEndDialog = true;
                return;
            }

            if (data.res == 'white') {
                if (this.myAccount.color == 'w') {
                    win = true;
                }
            }
            else if (data.res == 'black') {
                if (this.myAccount.color == 'b') {
                    win = true;
                }
            }

            if (win) {
                this.endDialogMsg = "The game is over, You won ! " + data.why;
            }
            else {
                this.endDialogMsg = "The game is over, You lose !" + data.why;
            }
            this.showEndDialog = true;
        }


    },
    created() {
        this.refreshBoard();
    },
    props: {
        clickable: {
            type: Boolean,
            default: true
        }
    },
    methods: {
        /**
         * Pieces management
         */
        //Refresh pieces array
        refreshPieces() {
            this.pieces = [];
            for (let x = 0; x < 8; x++) {
                for (let y = 0; y < 8; y++) {
                    if (this.board[y].charAt(x) !== "-") {
                        this.pieces.push({
                            val: this.board[y].charAt(x),
                            x: x + 1,
                            y: 8 - y
                        })
                    }
                }
            }
        },

        //Triggered when a cell is clicked ( cell emit )
        cellClick(values) {
            if (this.clickable) {
                if (this.isPieceSelected) {
                    this.unSelectPiece();
                } else {
                    this.makeLegalMoves(values);
                }
            }
        },

        //Make moves array, triggered when cell clicked or img dragged
        makeLegalMoves(values) {
            this.moves = [];
            this.pieceSelected = { x: values.x, y: values.y };
            this.$socket.emit("chess", {
                type: "getAuthorizedPosition",
                pieceSelected: this.pieceSelected
            });
        },

        leaveRoom() {
            this.$store.commit("room/leave");
            this.$socket.emit("leaveRoom");

        },

        //Triggered when a legal move is clicked
        moveClick(values) {
            if (this.clickable) {
                for (let mve of this.moves) {
                    if (Number(mve.x) === Number(values.x) && Number(mve.y) === Number(values.y))        //Check pour drag & drop si le déplacement est autorisé
                    {
                        if (mve.promotion) {
                            let promotions = [];
                            for (let i of this.moves) {
                                if (Number(mve.x) === Number(values.x) && Number(mve.y) === Number(values.y)) {
                                    if (i.promotion) {
                                        promotions.push(i.promotion);
                                    }
                                }
                            }
                            this.modal.move = mve;
                            this.modal.promotions = promotions;
                            this.modal.show = true;
                        } else {
                            this.$socket.emit("chess", {
                                type: "move",
                                pieceSelected: this.pieceSelected,
                                cellTargeted: mve
                            });
                            this.unSelectPiece();
                        }
                    }
                }
            }

        },

        promotionMove(promotionMove) {
            this.modal.show = false;
            this.$socket.emit("chess", {
                type: "move",
                pieceSelected: this.pieceSelected,
                cellTargeted: promotionMove.move,
                promotion: promotionMove.promotion,
            });
            this.unSelectPiece();
            this.refreshPieces();
        },

        //Unselect piece selected (if one) and clear moves array
        unSelectPiece() {
            if (this.clickable) {

                this.pieceSelected = { x: -1, y: -1 };
                this.moves = [];
            }
        },
        refreshBoard() {
            this.refreshPieces();
            this.$emit('computeBasicScore', this.pieces);
        }
    },
    computed: {
        isPieceSelected: function () {
            return this.pieceSelected.x !== -1 || this.pieceSelected.y !== -1
        },
        board: function () {
            return this.$store.state.room.game.board;
        },
        whichTurn: function () {
            return this.$store.state.room.game.turn;
        },
        infoCells: function () {
            let infos = []
            if (this.$store.state.room.game.lastMove != null) {
                infos.push({
                    x: this.$store.state.room.game.lastMove.from.x,
                    y: this.$store.state.room.game.lastMove.from.y,
                    lastMove: true,
                    bestMove: false
                });
                infos.push({
                    x: this.$store.state.room.game.lastMove.to.x,
                    y: this.$store.state.room.game.lastMove.to.y,
                    lastMove: true,
                    bestMove: false
                })
            }
            if (this.$store.state.room.game.bestMove != null) {

                infos.push({
                    x: this.$store.state.room.game.bestMove.from.x,
                    y: this.$store.state.room.game.bestMove.from.y,
                    lastMove: false,
                    bestMove: true
                }),
                    infos.push({
                        x: this.$store.state.room.game.bestMove.to.x,
                        y: this.$store.state.room.game.bestMove.to.y,
                        lastMove: false,
                        bestMove: true
                    });
            }
            return infos;
        },
        myAccount: function () {
            if (this.$store.state.room.guest.id === this.$store.state.user.user.user.userid) {
                return this.$store.state.room.guest;
            }
            return this.$store.state.room.host;
        }

    },
    watch: {
        board() {
            this.refreshBoard();
        },

    }
}