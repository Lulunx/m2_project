export default {
    data () {
      return {
        
      }
    },
    sockets:{
        snack:function(data){
            this.$store.commit("snack/updateMessage", data.feedback);
            this.$store.commit("snack/setColor", data.type);
            this.$store.commit("snack/show", true);
        }
    },
    computed: {
        show :{
            get(){
                return this.$store.state.snack.show;
            },
            set(){
                this.$store.commit("snack/show", false);
            }
        },
        message(){
            return this.$store.state.snack.message;
        },
        color(){
            return this.$store.state.snack.color;
        }
         
    }
  }