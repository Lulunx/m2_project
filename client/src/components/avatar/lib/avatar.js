export default {
	name: "avatar",
	data() {
		return {
			"lol": ""
		};
	},
	components: {},
	methods: {},
	props: {
		nom: {
			type: String,
			default: "John Doe"
		},
		profession: {
			type: String,
			default: "Web Developer"
		}
	}
};