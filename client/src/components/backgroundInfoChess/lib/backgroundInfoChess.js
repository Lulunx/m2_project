
export default {
    name: "backgroundInfoChess",
    components: {

    },
    data() {
        return {

        }
    },
    props: {
        lastMove: Boolean,
        bestMove: Boolean
    },
    sockets: {

    },
    methods: {

    },
    computed: {
        conditionalCSS() {
            return {
                'lastmove': this.lastMove,
                'bestmove': this.bestMove
            }
        },
        isGuest() {
            return this.$store.state.room.guest.id === this.$store.state.user.user.user.userid;
        },
        isItMyTurn() {
            let currentTurn = this.$store.state.room.game.turn;
            if (this.isGuest) {
                return currentTurn === this.$store.state.room.guest.color;
            }
            return currentTurn === this.$store.state.room.host.color;
        }
    }
}