import {extend, ValidationObserver, ValidationProvider} from 'vee-validate';
import {confirmed, email, min, required} from 'vee-validate/dist/rules';

extend('required', {
    ...required,
    message: 'This field is required'
});
extend('email', {
    ...email,
    message: 'You must use a valid email'
});
extend('confirmed', {
    ...confirmed,
    message: 'Password must match'
});
extend('min', {
    ...min,
    message: 'Not enough characters'
});

export default {
    name: "signupform",
    components: {
        ValidationProvider,
        ValidationObserver
    },
    data() {
        return {
            "show": false,
            "valid": false,
            "data": {
                "pseudo": "",
                "email": "",
                "password": "",
            },
            "confirmation": ""
        }
    },
    methods: {
        onSubmit() {
            fetch(this.$store.state.apiAddress + "users/register",
                {
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    method: "POST",
                    body: JSON.stringify(this.data)
                })
                .then(res => res.json()
                    .then(res => {
                        if (res.status >= 400) {
                            this.$store.commit("snack/updateMessage", res.message);
                            this.$store.commit("snack/setColor", "error");
                            this.$store.commit("snack/show", true);
                        }
                        else {
                            this.$store.commit("snack/updateMessage", "Account created !");
                            this.$store.commit("snack/setColor", "success");
                            this.$store.commit("snack/show", true);
                            this.$router.push({name: "SignIn"})

                        }
                    }));
        }
    }
}