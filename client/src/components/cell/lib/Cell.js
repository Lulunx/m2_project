
export default {
    name: "Cell",
    components: {

    },
    data() {
        return {
            pieceTypes: [
                {
                    val: "P",
                    img: require("../../../assets/chess_pieces/white_pawn.png")
                },
                {
                    val: "R",
                    img: require("../../../assets/chess_pieces/white_rook.png")
                },
                {
                    val: "N",
                    img: require("../../../assets/chess_pieces/white_knight.png")
                },
                {
                    val: "B",
                    img: require("../../../assets/chess_pieces/white_bishop.png")
                },
                {
                    val: "Q",
                    img: require("../../../assets/chess_pieces/white_queen.png")
                },
                {
                    val: "K",
                    img: require("../../../assets/chess_pieces/white_king.png")
                },
                {
                    val: "p",
                    img: require("../../../assets/chess_pieces/black_pawn.png")
                },
                {
                    val: "r",
                    img: require("../../../assets/chess_pieces/black_rook.png")
                },
                {
                    val: "n",
                    img: require("../../../assets/chess_pieces/black_knight.png")
                },
                {
                    val: "b",
                    img: require("../../../assets/chess_pieces/black_bishop.png")
                },
                {
                    val: "q",
                    img: require("../../../assets/chess_pieces/black_queen.png")
                },
                {
                    val: "k",
                    img: require("../../../assets/chess_pieces/black_king.png")
                }
            ],
            "a": 0,
            startX: null,
            startY: null,
            lastTranslateX: 0,
            translateX: 0,
            lastTranslateY: 0,
            translateY: 0,
            cellSize: 60,
        }
    },
    props: {
        x: Number,
        y: Number,
        val: String,
        clickable: {
            type: Boolean,
            default: true
        }
    },
    sockets: {

    },
    methods: {
        clickCell() {
            if (this.clickable) {
                if (this.isItMyTurn) {
                    this.$emit('cellClick', { x: this.x, y: this.y, val: this.val });
                }
            }
        },

        /**
         * Drag section
         */
        dragStart(event) {
            this.$emit('dragImgStart', { x: this.x, y: this.y, val: this.val });
            this.startX = event.screenX;
            this.startY = event.screenY;
            this.cellSize = event.toElement.style.width.substring(0, event.toElement.style.width.length - 2);
        },
        dragEnd() {
            this.translateX = this.lastTranslateX;
            this.translateY = this.lastTranslateY;
            this.$emit("dragImgEnd", { x: Math.ceil(this.translateX / this.cellSize), y: Math.ceil((this.translateY * -1) / this.cellSize) })
        },
        dragMove(event) {
            if (event.screenX === 0) return;

            let distanceX = event.screenX - this.startX;
            let distanceY = event.screenY - this.startY;

            let newTranslateX = this.translateX + distanceX;
            let newTranslateY = this.translateY + distanceY;
            newTranslateX += this.cellSize * this.x;
            newTranslateY -= this.cellSize * this.y;

            if (newTranslateX < -20 || newTranslateX > 8 * this.cellSize + 20) return;
            if (newTranslateY > 20 || newTranslateY < -8 * this.cellSize - 20) return;


            this.lastTranslateX = newTranslateX;
            this.lastTranslateY = newTranslateY;
            this.$refs.cell.style.transform = `translate(${newTranslateX}px, ${newTranslateY}px)`;
        }
    },
    computed: {
        isGuest() {
            return this.$store.state.room.guest.id === this.$store.state.user.user.user.userid;
        },
        isItMyTurn() {
            let currentTurn = this.$store.state.room.game.turn;
            if (this.isGuest) {
                return currentTurn === this.$store.state.room.guest.color;
            }
            return currentTurn === this.$store.state.room.host.color;
        },
        isCellCheck() {
            return (
                (this.val == 'K' && this.$store.state.room.game.turn == 'w') ||
                (this.val == 'k' && this.$store.state.room.game.turn == 'b'))
                && this.$store.state.room.game.isCheck;
        },
        conditionalCSS() {
            return {
                'check': this.isCellCheck,
            }
        }
    },
    watch: {
        translateX(value) {
            this.$refs.cell.style.transform = `translate(${value}px, ${this.translateY}px)`;
        },
        translateY(value) {
            this.$refs.cell.style.transform = `translate(${this.translatex}px, ${value}px)`;
        }
    }
}