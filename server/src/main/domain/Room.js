'use strict';

class Room {

    constructor(id, host, fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1") {
        this._id = id;
        this._players = [];
        this._host = host;
        this._guestReady = false;
        this._started = false;
        this._chessEngine = null;
        this._fen = fen;
    }

    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
    }

    get fen() {
        return this._fen;
    }

    set fen(value) {
        this._fen = value;
    }

    get chessEngine() {
        return this._chessEngine;
    }

    get players() {
        return this._players;
    }

    set players(value) {
        return this._players = value;
    }

    get host() {
        return this._host;
    }

    set host(value) {
        this._host = value;
    }

    get guestReady() {
        return this._guestReady;
    }

    set guestReady(value) {
        this._guestReady = value;
    }

    get started() {
        return this._started;
    }

    set started(value) {
        this._started = value;
    }

    get chessEngine() {
        return this._chessEngine;
    }

    set chessEngine(value) {
        this._chessEngine = value;
    }

    isEmpty() {
        return this.players.length === 0;
    }

    isFull() {
        return this.players.length === 2;
    }

    playerJoin(player) {
        if (!this.isFull()) {
            this.players.push(player);
            return 1;
        }
        return 0;
    }

    playerLeave(player) {
        let i;
        this.players.forEach((value, index) => {
            if (player.clientid === value.clientid) {
                i = index;
            }
        });
        if (i !== null) {
            this.players.splice(i, 1);
        }
    }
}

module.exports = Room;
