'use strict';

class Player {

    constructor(clientid, playerid, name) {
        this._playerid = playerid;
        this._clientid = clientid;
        this._name = name;
        this._room = -1;
    }

    get playerid() {
        return this._playerid;
    }

    set playerid(value) {
        this._playerid = value;
    }

    get clientid() {
        return this._clientid;
    }

    set clientid(value) {
        this._clientid = value;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get room() {
        return this._room;
    }

    set room(value) {
        this._room = value;
    }

    isFreeToPlay() {
        return this.room === -1;
    }
}

module.exports = Player;
