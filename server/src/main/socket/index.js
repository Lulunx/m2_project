/*
    Event to bind after Authentication
 */
exports.msg = require('./events/msg');
exports.joinRoom = require('./events/joinRoom');
exports.leaveRoom = require('./events/leaveRoom');
exports.createRoom = require('./events/createRoom');
exports.resumeGame = require('./events/resumeGame');
exports.declinedInvitation = require('./events/declinedInvitation');
exports.resumingGame = require('./events/resumingGame');