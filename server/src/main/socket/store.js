'use strict';

exports.store = {
    rooms: {
        lastId: 0,
        map: new Map()
    },
    players: new Map()
};