'use strict';

exports.bindEvent = (socket, {name, fn}) => {
    socket.on(name, (data) => {
        return fn(socket, data);
    })
};

exports.createEvent = function (name, fn) {
    return {
        name,
        fn
    };
};

