// TODO REVIEW

'use strict';

const SocketHelper = require('../helpers');

const chess = require('./chess');

const {playersService, roomsService} = require('../../services/index');

const io = require('../../../main/server');

const db = require('../../database/conn');


const GameData = require('../../database/GameData');

const gameData = new GameData(db);

const declinedInvitation = SocketHelper.createEvent(
    "declinedInvitation",
    (socket, data) => {
        if (data.sendTo && data.data) {
            let whiteHostPlayer = playersService.getPlayerById(data.sendTo);
            if (whiteHostPlayer) {
                socket.broadcast.to(whiteHostPlayer.clientid).emit("declinedInvitation", data.data);
            }
        }
    }
);

module.exports = declinedInvitation;