// TODO REVIEW

'use strict';

const SocketHelper = require('../helpers');

const chess = require('./chess');

const { playersService, roomsService } = require('../../services/index');

const io = require('../../../main/server');

const db = require('../../database/conn');


const GameData = require('../../database/GameData');

const gameData = new GameData(db);

const resumingGame = SocketHelper.createEvent(
    "resumingGame",
    (socket, data) => {
        console.log(data);
        let whiteHostPlayer = playersService.getPlayerById(data.GAME_WHITE_USER);
        if (whiteHostPlayer && whiteHostPlayer.isFreeToPlay()) {
            let blackGuestPlayer = playersService.getPlayerById(data.GAME_BLACK_USER);
            if (blackGuestPlayer && blackGuestPlayer.isFreeToPlay()) {
                let room = roomsService.createRoom(whiteHostPlayer);
                roomsService.addPlayerToRoom(whiteHostPlayer, room.id);
                roomsService.addPlayerToRoom(blackGuestPlayer, room.id);
                playersService.playerJoinRoom(whiteHostPlayer, room.id);
                playersService.playerJoinRoom(blackGuestPlayer, room.id);

                let sockets = io.io.of('/').connected;

                SocketHelper.bindEvent(sockets[whiteHostPlayer.clientid], chess);
                SocketHelper.bindEvent(sockets[blackGuestPlayer.clientid], chess);


                // recreate the game from database
                console.log("recreating game");
                gameData.getAllTurnByGameId(data.GAME_ID).then(
                    (res) => {
                        console.log(res);
                        let moves = "";
                        let chessJsMoves = [];
                        for (const move of res) {
                            chessJsMoves.push(uiToChessJsFormat(
                                move.TURN_MOVE.substr(0, 2),
                                move.TURN_MOVE.substr(2, 2),
                                move.TURN_MOVE.length > 4 ? move.TURN_MOVE.substr(4, 1) : null
                            ));
                            moves = moves.concat(" " + move.TURN_MOVE);
                        }
                        roomsService.resumeGame(room, 500, 500, moves, chessJsMoves, data.GAME_ID);
                    }
                );


                socket.broadcast.to(whiteHostPlayer.clientid).emit("roomCreated", {
                    "roomid": room.id,
                    "host": { "name": whiteHostPlayer.name, "id": whiteHostPlayer.playerid },
                    "fen": ""
                });

                let dataRes = {
                    "roomid": room.id,
                    "host": { "name": whiteHostPlayer.name, "id": whiteHostPlayer.playerid },
                    "guest": { "name": blackGuestPlayer.name, "id": blackGuestPlayer.playerid }
                };
                socket.emit("guestJoinedRoom", dataRes);
                socket.broadcast.to(whiteHostPlayer.clientid).emit("guestJoinedRoom", dataRes);
                
                console.log(`Game Resuming for room ${room.id}`);

                socket.emit("gameStarting");
                for (const player of room.players) {
                    SocketHelper.bindEvent(sockets[player.clientid], chess);
                    if (player.clientid !== socket.id) {
                        socket.broadcast.to(player.clientid).emit("gameStarting");
                    }
                }
            } else {
                console.log(`resumeGame : failed -> Guest Player is not online/free`)
                // TODO emit for a feedback
            }
        } else {
            console.log(`resumeGame : failed -> Player is not registered`);
            // TODO emit for a feedback
        }
    }
);

function uiToChessJsFormat(pieceSelected, cellTargeted, promotion) {
    if (promotion) {
        return {
            from: pieceSelected,
            to: cellTargeted,
            promotion: promotion
        }
    } else {
        return {
            from: pieceSelected,
            to: cellTargeted
        }
    }
}

module.exports = resumingGame;