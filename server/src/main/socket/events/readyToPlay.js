// TODO REVIEW

'use strict';

const SocketHelper = require('../helpers');

const {playersService, roomsService} = require('../../services/index');

const readyToPlay = SocketHelper.createEvent(
    "readyToPlay",
    (socket) => {
        let player = playersService.getPlayerByClientId(socket.id);
        if (player) {
            let room = roomsService.getRoomByRoomId(player.room);
            if (room) {
                roomsService.guestIsReady(room);
                socket.emit("guestIsReady");
                for (const player of room.players) {
                    socket.broadcast.to(player.clientid).emit("guestIsReady");
                }
            }
        }
    }
);

module.exports = readyToPlay;