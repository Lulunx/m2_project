// TODO REVIEW

'use strict';

const SocketHelper = require('../helpers');

const {playersService, roomsService} = require('../../services/index');

const msg = SocketHelper.createEvent(
    "msg",
    (socket, data) => {
        console.log("event : msg");
        console.log(data);
        if (data.msg && data.author) {
            let player = playersService.getPlayerByClientId(socket.id);
            if (player) {
                let room = roomsService.getRoomByRoomId(player.room);
                if (room) {
                    for (const player of room.players) {
                        //send to other people in the room
                        socket.broadcast.to(player.clientid).emit("msg", data);
                    }
                    // send to himself
                    socket.emit("msg", data);
                }
            }
        }
    }
);

module.exports = msg;