// TODO REVIEW

'use strict';

const SocketHelper = require('../helpers');

const db = require('../../database/conn');

const FenParser = require("@chess-fu/fen-parser");

const GameData = require('../../database/GameData');

const gameData = new GameData(db);

const io = require('../../server')



const { playersService, roomsService } = require('../../services/index');

const columns = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];

const chess = SocketHelper.createEvent(
    "chess",
    async (socket, data) => {
        const sockets = io.io.of('/').connected;

        let player = playersService.getPlayerByClientId(socket.id);
        if (player) {
            let room = roomsService.getRoomByRoomId(player.room);
            if (room && room.chessEngine) {
                switch (data.type) {
                    case "leaveRoom":
                        break;
                    case "forfeit":
                        let forfeitData = {
                            res: "",
                            why: "Forfeit",
                        };

                        forfeitData.res = player.playerid === room.host.playerid ? "black" : "white";

                        // Game Over
                        for (const player of room.players) {
                            sockets[player.clientid].emit("endGame", forfeitData);

                        }
                        gameData.updateGame(['END', forfeitData.res.toUpperCase() + "_FORFEIT", room.chessEngine.gameid]);
                        break;
                    case "timeout":

                        let fenParser = new FenParser.FenParser(room.chessEngine.lastFen);
                        let oldTurn = fenParser.turn;

                        let date = new Date();
                        let timeMove = Math.round((Number(date.getTime()) - Number(room.chessEngine.lastMoveTimeStamp)) / 1000);
                        
                        let newBoardData = {
                            fen: room.chessEngine.lastFen,
                            lastMove: room.chessEngine.lastMove && room.chessEngine.lastMove.length >= 4 ? chessEngineToUiFormat(room.chessEngine.lastMove) : null,
                            incheck: room.chessEngine.chessJs.in_check(),
                            timers: {
                                host: room.chessEngine.timeHost,
                                guest: room.chessEngine.timeGuest
                            }
                        };
                        console.log("BEFORE TIMER HOST", newBoardData.timers.host)
                        console.log("BEFORE TIMER guest", newBoardData.timers.guest)
                        
                        if (oldTurn === 'w') {
                            newBoardData.timers.host -= timeMove;
                        } else {
                            newBoardData.timers.guest -= timeMove;
                        }

                        console.log("AFTER TIMER HOST", newBoardData.timers.host)
                        console.log("AFTER TIMER guest", newBoardData.timers.guest)


                        let gameOverData = {
                            res: "",
                            why: "",
                        };
                        let gameOver = false;
                        if (newBoardData.timers.host <= 0) {
                            gameOver = true;
                            gameOverData.res = "black";
                            gameOverData.why = "No more time";
                        } else if (newBoardData.timers.guest <= 0) {
                            gameOver = true;
                            gameOverData.res = "white";
                            gameOverData.why = "No more time";
                        }

                        if (gameOver) {
                            // Game Over
                            for (const player of room.players) {
                                sockets[player.clientid].emit("endGame", gameOverData);

                            }
                            gameData.updateGame(['END', gameOverData.res.toUpperCase(), room.chessEngine.gameid]);
                        } else {
                            // Send again information to refresh
                            for (const player of room.players) {
                                sockets[player.clientid].emit("newBoard", newBoardData);
                            }
                        }
                        break;
                    case "getBestMove":
                        console.log("Event received: Chess getBestMove");
                        console.log(room.chessEngine.bestNextMove);
                        socket.emit("getBestMove", room.chessEngine.bestNextMove);
                        break;
                    case "move":
                        console.log("Event received: Chess move");
                        if (data.promotion) {
                            room.chessEngine.chessJs.move(uiToChessJsFormat(data.pieceSelected, data.cellTargeted, data.promotion));
                            room.chessEngine.lastMove = uiToChessFormat(data.pieceSelected, data.cellTargeted).concat(data.promotion);
                        } else {
                            room.chessEngine.chessJs.move(uiToChessJsFormat(data.pieceSelected, data.cellTargeted, null));
                            room.chessEngine.lastMove = uiToChessFormat(data.pieceSelected, data.cellTargeted);
                        }
                        room.chessEngine.lastMoveToBeRegister = true;
                        room.chessEngine.command(`position fen ${room.chessEngine.lastFen} moves ${room.chessEngine.lastMove}`);
                        room.chessEngine.command('go depth 5');
                        room.chessEngine.command('d');
                        break;
                    case "getAuthorizedPosition":
                        let pieceSelected = columns[data.pieceSelected.x - 1].concat(data.pieceSelected.y);
                        let legalsMoves = room.chessEngine.moves;
                        let res = [];
                        for (const move of legalsMoves) {
                            if (move.substr(0, 2) === pieceSelected) {
                                res.push(chessEngineToUiFormatOld(move));
                            }
                        }
                        socket.emit("authorizedPosition", res);
                        break;
                }
            }
        }
    }
);


function chessEngineToUiFormatOld(move) {
    if (move.length <= 4) {
        return {
            x: Number(columns.indexOf(move.charAt(2)) + 1),
            y: Number(move.charAt(3))
        }
    } else {
        return {
            x: Number(columns.indexOf(move.charAt(2)) + 1),
            y: Number(move.charAt(3)),
            promotion: move.charAt(4)
        }
    }
}

function chessEngineToUiFormat(move) {
    console.log(move);
    return {
        from: {
            x: Number(columns.indexOf(move.charAt(0)) + 1),
            y: Number(move.charAt(1))
        },
        to: {
            x: Number(columns.indexOf(move.charAt(2)) + 1),
            y: Number(move.charAt(3))
        }
    };
}
function uiToChessFormat(pieceSelected, cellTargeted) {
    return columns[pieceSelected.x - 1] + pieceSelected.y + columns[cellTargeted.x - 1] + cellTargeted.y;

}

function uiToChessJsFormat(pieceSelected, cellTargeted, promotion) {
    console.log(pieceSelected);
    console.log(cellTargeted);
    console.log(promotion);
    if (promotion) {
        return {
            from: columns[pieceSelected.x - 1] + pieceSelected.y,
            to: columns[cellTargeted.x - 1] + cellTargeted.y,
            promotion: promotion
        }
    } else {
        return {
            from: columns[pieceSelected.x - 1] + pieceSelected.y,
            to: columns[cellTargeted.x - 1] + cellTargeted.y
        }
    }
}

module.exports = chess;

