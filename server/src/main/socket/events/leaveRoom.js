'use strict';

const SocketHelper = require('../helpers');

const {playersService, roomsService} = require('../../services/index');

const leaveRoom = SocketHelper.createEvent(
    "leaveRoom",
    (socket) => {
        console.log(`Received event: leaveRoom`);

        // Search for registered player
        let player = playersService.getPlayerByClientId(socket.id);
        if (player) {
            let roomid = player.room;

            // Handling leaving players
            let room = roomsService.getRoomByRoomId(roomid);
            if (room) {
                // TODO change that :
                roomsService.removePlayerFromRoom(roomid, player);
                // TODO if the game has started eject everyone
                // TODO If the host is leaving, eject guest
                // TODO else just eject the guest
            } else {
                // TODO rework client side "on" "room" for leaving players
                // TODO to make sure the store is nicely reset
                socket.emit("room", -1);
            }

            // Then we update the player object
            // TODO update BOTH players objects
            playersService.playerLeaveRoom(player);

            // TODO others emits to handles every cases
            socket.emit("playerHasLeft");
            console.log(`Done successfully: Player ${player.name} (${player.playerid}) has left Room ID = ${roomid}`)
        } else {
            console.log('Error: Player not registered tried to leave a room !');
            console.log('Error: He should not have been allowed to be in a room in the first place');
            socket.emit("snack", {
                type: "error",
                feedback: "Error has occurred, try refresh the page"
            });
            // TODO rework client side "on" "room" for leaving players
            // TODO to make sure the store is nicely reset
            socket.emit("room", -1);
        }
    }
);

module.exports = leaveRoom;
