'use strict';

const SocketHelper = require('../helpers');

const readyToPlay = require('./readyToPlay');

const {playersService, roomsService} = require('../../services/index');

const joinRoom = SocketHelper.createEvent(
    "joinRoom",
    (socket, data) => {
        console.log(`Received event: createRoom`);

        // Check if data is fulfilled
        if (data.room_id) {
            let roomid;
            try {
                roomid = Number(data.room_id);
            } catch (e) {
                socket.emit("snack", {
                    type: "error",
                    feedback: "Provided room ID is not valid"
                });
                console.log("Failed: Provided room ID is not valid")
            }

            // Search if the room exist
            if (roomid && roomsService.getRoomByRoomId(roomid)) {

                // Search for registered player
                let player = playersService.getPlayerByClientId(socket.id);
                if (player) {

                    // Add player to the room
                    let room = roomsService.addPlayerToRoom(player, roomid);
                    if (room) {
                        // Successfully added then we update the player object
                        playersService.playerJoinRoom(player, roomid);

                        // Then we allow the guest player to notify when he is ready
                        SocketHelper.bindEvent(socket, readyToPlay);

                        // Emit to update both clients
                        let data = {
                            "roomid": roomid,
                            "host": {"name": room.host.name, "id": room.host.playerid},
                            "guest": {"name": player.name, "id": player.playerid},
                            "fen": room.fen
                        };
                        socket.emit("guestJoinedRoom", data);
                        socket.broadcast.to(room.host.clientid).emit("guestJoinedRoom", data);
                    } else {
                        socket.emit("snack", {
                            type: "error",
                            feedback: `Room ${roomid} is full`
                        });
                        console.log(`Failed: Room ${roomid} is full`)
                    }
                } else {
                    socket.emit("snack", {
                        type: "error",
                        feedback: `Please refresh the page and try again`
                    });
                    console.log("Failed: Player not registered");
                }
            } else {
                socket.emit("snack", {
                    type: "error",
                    feedback: `Room ${roomid} does not exist`
                });
                console.log(`Failed: Room ${roomid} does not exist`);
            }
        }
    }
);

module.exports = joinRoom;