// TODO REVIEW

'use strict';

const SocketHelper = require('../helpers');

const {playersService} = require('../../services/index');

const resumeGame = SocketHelper.createEvent(
    "resumeGame",
    (socket, data) => {
        console.log(data);
        let whiteHostPlayer = playersService.getPlayerByClientId(socket.id);
        if (whiteHostPlayer) {
            let blackGuestPlayer = playersService.getPlayerById(data.GAME_BLACK_USER);
            console.log(blackGuestPlayer);
            if (blackGuestPlayer && blackGuestPlayer.isFreeToPlay()) {
                socket.broadcast.to(blackGuestPlayer.clientid).emit("invitationToResumeAGame", data);
            } else {
                socket.emit("snack", {
                    type: "error",
                    feedback: `${blackGuestPlayer.name} is not free to play`
                });
                console.log(`resumeGame : failed -> Guest Player is not online/free`)
            }
        } else {
            socket.emit("snack", {
                type: "error",
                feedback: "Error has occurred, try refresh the page"
            });
            console.log(`resumeGame : failed -> Player is not registered`);
        }
    }
);

module.exports = resumeGame;