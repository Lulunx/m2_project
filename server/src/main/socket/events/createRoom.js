'use strict';
const FenParser = require("@chess-fu/fen-parser");

const SocketHelper = require('../helpers');

const startGame = require('./startGame');

const { playersService, roomsService } = require('../../services/index');

const createRoom = SocketHelper.createEvent(
    "createRoom",
    (socket, data) => {
        console.log(`Received event: createRoom`);

        // Search for registered player
        let player = playersService.getPlayerByClientId(socket.id);
        if (player) {
            if (player.isFreeToPlay()) {
                let room;
                if (data) {
                    if (data.fen) {
                        if (FenParser.FenParser.isFen(data.fen)) {

                            // Create room with starting positions
                            room = roomsService.createRoom(player, data.fen);
                        } else {
                            socket.emit("snack", {
                                type: "error",
                                feedback: "Invalid FEN"
                            });
                        }
                    } else {
                        room = roomsService.createRoom(player);
                    }
                }
                if (room) {
                    roomsService.addPlayerToRoom(player, room.id);
                    playersService.playerJoinRoom(player, room.id);

                    SocketHelper.bindEvent(socket, startGame);

                    console.log(`Done successfully: Room ID = ${room.id}`);

                    // Emit to update client
                    socket.emit("roomCreated", {
                        "roomid": room.id,
                        "host": {"name": player.name, "id": player.playerid},
                        "fen": room.fen
                    });
                }
            } else {
                console.log('Error: Player not free tried to create a room !');
            }
        } else {
            socket.emit("snack", {
                type: "error",
                feedback: "Room creation failed, try to refresh the page"
            });
            console.log("Failed: Player not registered");
        }
    }
);

module.exports = createRoom;
