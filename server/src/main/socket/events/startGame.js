// TODO REVIEW

'use strict';

const SocketHelper = require('../helpers');

const chess = require('./chess');

const {playersService, roomsService} = require('../../services/index');

const io = require('../../../main/server');

const db = require('../../database/conn');


const GameData = require('../../database/GameData');

const gameData = new GameData(db);

const startGame = SocketHelper.createEvent(
    "startGame",
    (socket, data) => {
        console.log(`Received event: startGame`);

        let playerTime = data.playerTime;
        let player = playersService.getPlayerByClientId(socket.id);
        if (player) {
            let room = roomsService.getRoomByRoomId(player.room);
            if (room) {
                if (room.guestReady) {
                    roomsService.startGame(room, playerTime);

                    let sockets = io.io.of('/').connected;

                    console.log(`Game Starting for room ${room.id}`);

                    socket.emit("gameStarting");
                    for (const player of room.players) {
                        SocketHelper.bindEvent(sockets[player.clientid], chess);
                        if (player.clientid !== socket.id) {
                            socket.broadcast.to(player.clientid).emit("gameStarting");
                        }
                    }

                    // il faut passer en data un array [ white player id, black player id ]
                    // là je map comme un gogole sur room.players et ça fonctionne
                    // car le host est toujours le blanc
                    console.log("register a new game in database");
                    gameData.createNewGame(room.players.map((player) => player.playerid)).then(
                        (res) => {
                            room.chessEngine.gameid = res.insertId;
                            console.log(`Game ID : ${room.chessEngine.gameid}`);
                        }
                    );

                } else {
                    socket.emit("guestNotReady");
                }

            }
        }
    }
);

module.exports = startGame;