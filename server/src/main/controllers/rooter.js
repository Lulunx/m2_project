'use strict';

const express = require('express');

const UsersController = require('./resources/UsersController');

const rooter = function () {
    const router = express.Router();
    router.use('/users', UsersController());
    return router;
};

module.exports = rooter;
