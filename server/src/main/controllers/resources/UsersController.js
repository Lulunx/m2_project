'use strict';

const express = require('express');
const HttpResponse = require('../../utils/HttpResponse');

const db = require('../../database/conn');
const UsersData = require('../../database/UsersData');

const bcrypt = require('bcryptjs');
//https://stackoverflow.com/questions/29320201/error-installing-bcrypt-with-npm
const jwt = require('jsonwebtoken');

const Settings = require('../../../../settings');

const UsersController = function () {
    const router = express.Router();

    const userData = new UsersData(db);

    /**
     * @swagger
     * /users/register:
     *  post:
     *      description: Create a new User
     *      requestBody:
     *          content:
     *              application/json:
     *                  schema:
     *                      type: object
     *                      properties:
     *                          pseudo:
     *                              type: string
     *                          password:
     *                              type: string
     *                          email:
     *                              type: string
     *                  example:
     *                      pseudo: Nykola
     *                      password: 123456789
     *                      email: 21402041@etu.unicaen.fr
     *      responses:
     *          '200':
     *              description: User created
     *              content:
     *                  application/json:
     *                      schema:
     *                          $ref: '#/components/schemas/HttpResponse'
     */
    router.post('/register', (req, res) => {
        if (req.body.pseudo && req.body.password && req.body.email) {

            userData.getUser(req.body.pseudo).then((rows) => {
                if (rows.length > 0) {
                    res.status(400);
                    let response = new HttpResponse(res.statusCode);
                    response.content = 'Unauthorized';
                    response.message = 'User already exists';
                    res.json(response.toJSON());
                } else {
                    userData.createUser([
                        req.body.pseudo,
                        bcrypt.hashSync(req.body.password.toString(), bcrypt.genSaltSync(10)),
                        req.body.email
                    ]).then((rows) => {
                        res.status(200);
                        let response = new HttpResponse(res.statusCode);
                        response.message = 'OK';
                        response.content = rows;
                        res.json(response.toJSON());
                    });
                }


            })
        } else {
            res.status(400);
            let response = new HttpResponse(res.statusCode);
            response.content = 'Unauthorized';
            response.message = 'Bad request';
            res.json(response.toJSON());
        }
    });

    /**
     * @swagger
     * /users/authenticate:
     *  post:
     *      description: Authenticate an user
     *      requestBody:
     *          content:
     *              application/json:
     *                  schema:
     *                      type: object
     *                      properties:
     *                          pseudo:
     *                              type: string
     *                          password:
     *                              type: string
     *                  example:
     *                      pseudo: Nykola
     *                      password: 123456789
     *      responses:
     *          '200':
     *              description: User informations
     *              content:
     *                  application/json:
     *                      schema:
     *                          $ref: '#/components/schemas/HttpResponse'
     */
    router.post('/authenticate', async (req, res) => {
        if (req.body.pseudo && req.body.password) {
            const user = await userData.getUser([req.body.pseudo]);
            if (user.length === 1 && user[0].USER_PASSWORD && bcrypt.compareSync(req.body.password.toString(), user[0].USER_PASSWORD)) {
                const token = jwt.sign({
                    sub: {
                        "_playerid": user[0].USER_ID,
                        "_name": user[0].USER_PSEUDO
                    }
                }, Settings.secret);
                res.status(200);
                let response = new HttpResponse(res.statusCode);
                response.message = 'Welcome, ' + user[0].USER_PSEUDO;
                response.content = {
                    user: {
                        userid: user[0].USER_ID,
                        pseudo: user[0].USER_PSEUDO,
                        email: user[0].USER_MAIL,
                        since: user[0].USER_DATE
                    },
                    token: token
                };
                await res.json(response.toJSON());
            } else {
                res.status(401);
                let response = new HttpResponse(res.statusCode);
                response.content = 'Unauthorized';
                response.message = "Authentication failed";
                await res.json(response.toJSON());
            }
        } else {
            res.status(400);
            let response = new HttpResponse(res.statusCode);
            response.message = 'Bad request';
            res.json(response.toJSON());
        }
    });

    /**
     * @swagger
     * /users/:
     *  get:
     *      description: Retrieves all users
     *      security:
     *          - bearerAuth:
     *              type: http
     *              scheme: bearer
     *              bearerFormat: JWT
     *      responses:
     *          '200':
     *              description: List of all users
     *              content:
     *                  application/json:
     *                      schema:
     *                          $ref: '#/components/schemas/HttpResponse'
     */
    router.get('/', (req, res) => {
        userData.getUsers().then((rows) => {
            res.status(200);
            let response = new HttpResponse(res.statusCode);
            response.message = 'OK';
            response.content = rows;
            res.json(response.toJSON());
        });
    });

    /**
     * @swagger
     * /users/{id}/games/inprogress:
     *  get:
     *      description: Retrieves unfinished games created bu the user
     *      parameters:
     *          - in: path
     *            name: User ID
     *            schema:
     *              type: Integer
     *              example: 1
     *            description: User ID
     *      security:
     *          - bearerAuth:
     *              type: http
     *              scheme: bearer
     *              bearerFormat: JWT
     *      responses:
     *          '200':
     *              description: Unfinished games created by the user
     *              content:
     *                  application/json:
     *                      schema:
     *                          $ref: '#/components/schemas/HttpResponse'
     */
    router.get('/:id/games/inprogress', (req, res) => {
        userData.getInProgressGamesByUserId(req.params.id).then((rows) => {
            res.status(200);
            let response = new HttpResponse(res.statusCode);
            response.message = 'OK';
            response.content = rows;
            res.json(response.toJSON());
        });
    });

    /**
     * @swagger
     * /users/{id}/historical:
     *  get:
     *      description: List of games done by a user
     *      parameters:
     *          - in: path
     *            name: User ID
     *            schema:
     *              type: Integer
     *              example: 1
     *            description: User ID
     *      security:
     *          - bearerAuth:
     *              type: http
     *              scheme: bearer
     *              bearerFormat: JWT
     *      responses:
     *          '200':
     *              description: List of games done by a user
     *              content:
     *                  application/json:
     *                      schema:
     *                          $ref: '#/components/schemas/HttpResponse'
     */
    router.get('/:id/historical', (req, res) => {
        userData.getHistoricalByID(req.params.id).then((rows) => {
            res.status(200);
            let response = new HttpResponse(res.statusCode);
            response.message = 'OK';
            response.content = rows;
            res.json(response.toJSON());
        });
    });


    /**
     * @swagger
     * /users/{id}/stats:
     *  get:
     *      description: User statistics
     *      parameters:
     *          - in: path
     *            name: User ID
     *            schema:
     *              type: Integer
     *              example: 1
     *            description: User ID
     *      security:
     *          - bearerAuth:
     *              type: http
     *              scheme: bearer
     *              bearerFormat: JWT
     *      responses:
     *          '200':
     *              description: User statistics
     *              content:
     *                  application/json:
     *                      schema:
     *                          $ref: '#/components/schemas/HttpResponse'
     */
    router.get('/:id/stats', (req, res) => {
        userData.getStatsByID(req.params.id).then((rows) => {
            res.status(200);
            let response = new HttpResponse(res.statusCode);
            response.message = 'OK';
            response.content = rows;
            res.json(response.toJSON());
        });
    });
    return router;

};

module.exports = UsersController;
