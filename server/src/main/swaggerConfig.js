module.exports = {
    definition: {
        openapi: '3.0.0', // Specification (optional, defaults to swagger: '2.0')
        info: {
            title: 'Chess Project Back-end', // Title (required)
            version: '1.0.0', // Version (required)
            contact: {
                name: 'Lucas Gancel & Nicolas Hoffmann'
            }
        },
        components: {
            securitySchemes: {
                bearerAuth: {
                    type: 'http',
                    scheme: 'bearer',
                    bearerFormat: 'JWT'
                }
            }
        }
    },
    tags: [
        {
            name: 'CRUD operations'
        }
    ],
    // Path to the API docs
    apis: [
        './src/main/controllers/resources/UsersController.js',
        './src/main/utils/HttpResponse.js'
    ]
};
