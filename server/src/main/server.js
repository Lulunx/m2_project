'use strict';
// Import settings
const Settings = require('../../settings');

// Express
const express = require('express');
const app = express();

// BodyParser
const BodyParser = require('body-parser');

app.use(BodyParser.json());
app.use(BodyParser.urlencoded({extended: true}));

//CORS
const cors = require('cors');
app.use(cors());

// Import Swagger
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

// Swagger settings
const options = require('./swaggerConfig');

const swaggerSpec = swaggerJSDoc(options);

// Swagger url
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

// JWT
const jwt = require('./helpers/jwt');
app.use(jwt());

// Routes
const rooter = require('./controllers/rooter');
app.use(rooter());

// Create Server
const server = require('http').createServer(app);

// Sockets
const io = require('socket.io')(server, {origins: '*:*'});
const socketioJwt = require('socketio-jwt');

// Sockets Events
const SocketHelper = require('./socket/helpers');
const SocketEvents = require('./socket/index');

const services = require('./services/index');
const Player = require('./domain/Player');

io.on('connection', socketioJwt.authorize({
    secret: Settings.secret,
    timeout: 15000
}))
    .on('authenticated', (client) => {
        console.log(`New client authenticated ID : ${client.id}`);
        // Extract the player from the decoded token
        let item = JSON.stringify(client.decoded_token);
        let data = JSON.parse(item).sub;
        data["_clientid"] = client.id;
        let player = Object.assign(new Player, data);

        // Search if this player is already connected
        let oldConnectedClient = services.playersService.getOldClientIdByPlayerId(player.playerid);
        if (oldConnectedClient) {
            // Request the oldClient to disconnect
            // TODO client side
            client.broadcast.to(oldConnectedClient).emit("close");
        }

        // Register player
        services.playersService.registerNewPlayer(client.id, player);

        // Bind events to each client
        Object.values(SocketEvents).forEach((event) => {
            SocketHelper.bindEvent(client, event);
        });
        client.on('disconnect', (reason) => {
            console.log(`Client ${client.id} disconnected : ${reason}`);
            // Get the player corresponding of the closed client
            let player = services.playersService.getPlayerByClientId(client.id);
            if (player) {
                // If the player is in a room
                if (player.room && player.room !== -1) {
                    services.roomsService.removePlayerFromRoom(player.room, player);
                    // TODO eject the other player (the game can be resume now)
                }
                // Unregister player
                services.playersService.unregisterPlayer(client.id);
            }
        })
    });

// Start
server.listen(Settings.express.port, () => {
    console.log(`Server running on port ${Settings.express.port}`)
});

exports.io = io;
