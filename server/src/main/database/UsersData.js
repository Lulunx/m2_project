'use strict';

class UsersData {

    constructor(dbconn) {
        this._db = dbconn;
    }

    get dbconn() {
        return this._db;
    }

    set dbconn(value) {
        this._db = value;
    }

    createUser(values) {
        return this.dbconn.makeQuery(`
            INSERT INTO USERS (USER_PSEUDO, USER_PASSWORD, USER_MAIL)
            VALUES (?, ?, ?);
        `, values);
    }

    getUser(values) {
        return this.dbconn.makeQuery(`
            SELECT *
            FROM USERS
            WHERE USER_PSEUDO = ?;
        `, values);
    }

    getUsers() {
        return this.dbconn.makeQuery(`
            SELECT USER_PSEUDO
            FROM USERS;
        `);
    }

    getInProgressGamesByUserId(userid) {
        return this.dbconn.makeQuery(`
        SELECT GAMES.*, white.USER_PSEUDO as GAME_WHITE_USERNAME, black.USER_PSEUDO as GAME_BLACK_USERNAME
        from GAMES
                 JOIN USERS white ON (GAMES.GAME_WHITE_USER = white.USER_ID)
                 JOIN USERS black ON (GAMES.GAME_BLACK_USER = black.USER_ID)
        WHERE (GAME_WHITE_USER = ` + userid + ` or GAME_BLACK_USER = ` + userid +`) And GAME_STATE = 'PROGRESS'
        `);
    }

    getHistoricalByID(id) {
        return this.dbconn.makeQuery(`
            SELECT GAMES.*, white.USER_PSEUDO as GAME_WHITE_USERNAME, black.USER_PSEUDO as GAME_BLACK_USERNAME
            from GAMES
                     JOIN USERS white ON (GAMES.GAME_WHITE_USER = white.USER_ID)
                     JOIN USERS black ON (GAMES.GAME_BLACK_USER = black.USER_ID)
            WHERE (GAME_WHITE_USER = ` + id + ` or GAME_BLACK_USER = ` + id +`) And GAME_STATE = 'END' LIMIT 10
        `);
    }

    getStatsByID(id) {
        return this.dbconn.makeQuery(
`
Select (select count(*) from GAMES where (((GAME_WINNER = 'WHITE' and GAME_WHITE_USER = ${id}) or (GAME_WINNER = 'BLACK' and GAME_BLACK_USER = ${id})) And GAME_STATE = 'END')) as 'WINS',
        (select count(*) from GAMES where (((GAME_WINNER = 'WHITE' and GAME_BLACK_USER = ${id}) or (GAME_WINNER = 'BLACK' and GAME_WHITE_USER = ${id})) And GAME_STATE = 'END')) as 'LOSES',

        (select count(*) from GAMES where (((GAME_WINNER = 'WHITE_FORFEIT' and GAME_WHITE_USER = ${id}) or (GAME_WINNER = 'BLACK_FORFEIT' and GAME_BLACK_USER = ${id})) And GAME_STATE = 'END')) as 'WINS_FORFEIT',
        (select count(*) from GAMES where (((GAME_WINNER = 'WHITE_FORFEIT' and GAME_BLACK_USER = ${id}) or (GAME_WINNER = 'BLACK_FORFEIT' and GAME_WHITE_USER = ${id})) And GAME_STATE = 'END')) as 'LOSES_FORFEIT',

        (select count(*) from GAMES where (GAME_WINNER = 'DRAW' and (GAME_BLACK_USER = ${id} or GAME_WHITE_USER = ${id}) And GAME_STATE = 'END')) as 'DRAW'

`);
    }

}

module.exports = UsersData;
