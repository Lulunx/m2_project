'use strict';

class GameData {

    constructor(dbconn) {
        this._db = dbconn;
    }

    get dbconn() {
        return this._db;
    }

    set dbconn(value) {
        this._db = value;
    }

    createNewGame(values) {
        return this.dbconn.makeQuery(`
            INSERT INTO GAMES (GAME_WHITE_USER, GAME_BLACK_USER, GAME_STATE, GAME_WINNER)
            VALUES (?, ?, 'PROGRESS', 'DRAW');
        `, values);
    }

    updateGame(values) {
        return this.dbconn.makeQuery(`
            UPDATE GAMES
            SET GAME_STATE  = ?,
                GAME_WINNER = ?
            WHERE GAME_ID = ?
        `, values);
    }

    registerTurn(values) {
        return this.dbconn.makeQuery(`
            INSERT INTO GAME_TURN (GAME_ID, TURN_NUMBER, TURN_STRING, TURN_MOVE)
            VALUES (?, ?, ?, ?)
        `, values);
    }

    getAllTurnByGameId(values) {
        return this.dbconn.makeQuery(`
            SELECT TURN_NUMBER, TURN_MOVE
            FROM GAME_TURN
            WHERE GAME_ID = ?
            ORDER BY GAME_TURN.TURN_NUMBER ASC
        `, values);
    }

}

module.exports = GameData;
