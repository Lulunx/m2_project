'use strict';

const settings = require('../../../settings');

const mariadb = require('mariadb');

const pool = mariadb.createPool({
    database: settings.db.database,
    host: settings.db.host,
    user: settings.db.username,
    password: settings.db.password,
    connectionLimit: 5
});

pool.makeQuery = async function (queryString, values = []) {
    let conn;
    try {
        conn = await pool.getConnection();
        return await conn.query(queryString, values);
    } catch (e) {
        throw e;
    } finally {
        if (conn) conn.release();
    }
};

module.exports = pool;
