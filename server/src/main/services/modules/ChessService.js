// TODO REVIEW

'use strict';
const FenParser = require("@chess-fu/fen-parser");


const stockfish = require("stockfish");

const ChessJs = require('chess.js');

const io = require('../../server');


const db = require('../../database/conn');


const GameData = require('../../database/GameData');

const {playersService} = require('../index');

const gameData = new GameData(db);

class ChessService {

    constructor(room) {
        this._stockfish = stockfish();
        this._stockfish._chessJs = new ChessJs.Chess(room.fen);
        this._stockfish.onmessage = this.responseHandler;
        this._stockfish._gameid = 0;
        this._stockfish._timeHost = 0;
        this._stockfish._timeGuest = 0;
        this._stockfish._lastMoveTimeStamp = "";
        this._stockfish._lastMove = "";
        this._stockfish._lastMoveToBeRegister = false;
        this._stockfish._lastFen = "";
        this._stockfish._score = 0;
        this._stockfish._moves = ["aa"];
        this._stockfish._room = room;
        this._stockfish._ready = false;
        this._stockfish._bestNextMove = null;
        this._stockfish._readyInterval = setInterval(() => {
            if (!this._stockfish._ready) {
                this._stockfish.postMessage("isready");
            }
        }, 200);
        this.command(`position fen ${room.fen}`);
        this.command(`d`)
    }

    get bestNextMove() {
        return this._stockfish._bestNextMove;
    }

    set bestNextMove(value) {
        this._stockfish._bestNextMove = value;
    }

    get lastMoveToBeRegister() {
        return this._stockfish._lastMoveToBeRegister;
    }

    set lastMoveToBeRegister(value) {
        this._stockfish._lastMoveToBeRegister = value;
    }

    get chessJs() {
        return this._stockfish._chessJs;
    }

    set chessJs(value) {
        this._stockfish._chessJs = value;
    }

    get ready() {
        return this._stockfish._ready;
    }

    set ready(value) {
        this._stockfish._ready = value;
    }


    get gameid() {
        return this._stockfish._gameid;
    }

    set gameid(value) {
        this._stockfish._gameid = value;
    }

    get lastMoveTimeStamp() {
        return this._stockfish._lastMoveTimeStamp;
    }

    set lastMoveTimeStamp(value) {
        this._stockfish._lastMoveTimeStamp = value;
    }

    get timeHost() {
        return this._stockfish._timeHost;
    }

    set timeHost(value) {
        this._stockfish._timeHost = value;
    }

    get timeGuest() {
        return this._stockfish._timeGuest;
    }

    set timeGuest(value) {
        this._stockfish._timeGuest = value;
    }

    get room() {
        return this._stockfish._room;
    }

    set room(value) {
        this._stockfish._room = value;
    }

    get lastFen() {
        return this._stockfish._lastFen;
    }

    set lastFen(value) {
        this._stockfish._lastFen = value;
    }

    get stockfish() {
        return this._stockfish;
    }

    set stockfish(value) {
        this._stockfish = value;
    }

    get lastMove() {
        return this._stockfish._lastMove;
    }

    set lastMove(value) {
        this._stockfish._lastMove = value;
    }

    get moves() {
        return this._stockfish._moves;
    }

    set moves(value) {
        this._stockfish._moves = value;
    }

    get score() {
        return this._stockfish._score;
    }

    set score(value) {
        this._stockfish._score = value;
    }

    command(cmdString) {
        this.ready = false;
        this.stockfish.postMessage(cmdString)
    }


    /*
        Callback
     */
    responseHandler(event) {
        try {
            let sockets = io.io.of('/').connected;
            if (event === "uciok") {
                console.log("Engine is ready")
            } else if (event === "readyok") {
                this._ready = true;
            } else if (event.substr(0, 3) === "Fen") {
                let fen = event.substr(5, event.length - 4);
                if (this._lastFen !== fen) {

                    let fenParser = new FenParser.FenParser(this._lastFen);
                    let oldTurn = fenParser.turn;

                    this._lastFen = fen;

                    fenParser = new FenParser.FenParser(this._lastFen);
                    let newTurn = fenParser.turn;

                    if (oldTurn !== newTurn) {
                        console.log("CHANGEMENT DE JOUEUR => New turn");
                        // Si changement de joueur
                        let date = new Date();

                        let timeMove = Math.round((Number(date.getTime()) - Number(this._lastMoveTimeStamp)) / 1000);
                        this._lastMoveTimeStamp = date.getTime();

                        if (oldTurn === 'w') {
                            // white -> HOST
                            this._timeHost = this._timeHost - timeMove;
                        } else {
                            // then -> GUEST
                            this._timeGuest = this._timeGuest - timeMove;
                        }

                        if (this._lastMoveToBeRegister) {
                            gameData.registerTurn([this._gameid, fenParser.moveNumber, this._lastFen, this._lastMove]);
                            this._lastMoveToBeRegister = false;
                        }

                    }
                }


                let res = {
                    fen: this._lastFen,
                    lastMove: this._lastMove && this._lastMove.length >= 4 ? chessEngineToUiFormat(this._lastMove) : null,
                    incheck: this._chessJs.in_check(),
                    timers: {
                        host: this._timeHost,
                        guest: this._timeGuest
                    }
                };

                let gameOverData = {
                    res: "",
                    why: "",
                };

                let gameOver = false;

                if (this._chessJs.game_over()) {
                    gameOver = true;
                    if (this._chessJs.in_checkmate()) {
                        gameOverData.res = this._chessJs.turn() !== "w" ? "white" : "black";
                        gameOverData.why = "Checkmate";

                    } else if (this._chessJs.in_draw()) {
                        gameOverData.res = "draw";
                        gameOverData.why = "Draw";
                    } else if (this._chessJs.in_stalemate()) {
                        gameOverData.res = "draw";
                        gameOverData.why = "Stalemate";
                    } else if (this._chessJs.in_threefold_repetition()) {
                        gameOverData.res = "draw";
                        gameOverData.why = "Threefold repetition";
                    } else if (this._chessHs.insufficient_material()) {
                        gameOverData.res = "draw";
                        gameOverData.why = "Insufficient material";
                    }
                } else if (this._timeHost <= 0) {
                    gameOver = true;
                    gameOverData.res = "black";
                    gameOverData.why = "No more time";
                } else if (this._timeGuest <= 0) {
                    gameOver = true;
                    gameOverData.res = "white";
                    gameOverData.why = "No more time";
                }
                if (gameOver) {
                    for (const player of this._room.players) {
                        sockets[player.clientid].emit("endGame", gameOverData);
                    }
                    gameData.updateGame(['END', gameOverData.res.toUpperCase(), this._gameid]);
                } else {
                    for (const player of this._room.players) {
                        sockets[player.clientid].emit("newBoard", res);
                    }
                }

                console.log(`The Fen is : ${fen}`);
            } else if (event.substr(0, 16) === "Legal uci moves:") {
                this._moves = event.substr(17, event.length - 17).split(" ");
                console.log(this._moves);
            } else if (event.substr(0, 21) === "info depth 5 seldepth") {
                console.log(event.split(" "));
                this._score = event.split(" ")[9];
                for (const player of this._room.players) {
                    sockets[player.clientid].emit("stockfishScore", this._score);
                }


            } else if (event.indexOf('+') !== -1 || event.indexOf('|') !== -1) {
                console.log(`${event}`);
            } else if (event.substr(0, 8) === "bestmove") {
                this._bestNextMove = chessEngineToUiFormat(event.split(" ")[1]);
                for (const player of this._room.players) {
                    sockets[player.clientid].emit("getBestMove", this._bestNextMove);
                }

            }
        } catch (e) {
            console.log(event);
        }

    }
}

const columns = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];

function chessEngineToUiFormat(move) {
    console.log(move);
    return {
        from: {
            x: Number(columns.indexOf(move.charAt(0)) + 1),
            y: Number(move.charAt(1))
        },
        to: {
            x: Number(columns.indexOf(move.charAt(2)) + 1),
            y: Number(move.charAt(3))
        }
    };
}

module.exports = ChessService;