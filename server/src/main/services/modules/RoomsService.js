// TODO REVIEW

'use strict';
const Room = require('../../domain/Room');

const ChessService = require('./ChessService');

class RoomsService {

    constructor(storeData) {
        this._storeData = storeData;
    }

    get storeData() {
        return this._storeData;
    }

    createRoom(host, fen="rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1") {
        let id = this.storeData.getNewRoomId();
        let room = new Room(id, host, fen);
        this.storeData.newRoom(room);
        return room;
    }

    guestIsReady(room) {
        room.guestReady = true;
        this.storeData.updateRoom(room.id, room);
    }

    guestIsNotReady(room) {
        room.guestReady(false);
        this.storeData.updateRoom(room.id, room);
    }

    startGame(room, timePerPlayer) {
        room.chessEngine = new ChessService(room);
        let date = new Date();
        room.chessEngine.lastMoveTimeStamp = date.getTime();
        room.chessEngine.timeHost = timePerPlayer;
        room.chessEngine.timeGuest = timePerPlayer;
        room.started = true;
        this.storeData.updateRoom(room.id, room);
    }

    async resumeGame(room, timeHostWhitePlayer, timeGuestBlackPlayer, pastMoves, chessJsMoves, gameID) {
        room.chessEngine = new ChessService(room);
        room.chessEngine.gameid = gameID;
        let date = new Date();
        room.chessEngine.lastMoveTimeStamp = date.getTime();
        room.chessEngine.timeHost = timeHostWhitePlayer;
        room.chessEngine.timeGuest = timeGuestBlackPlayer;
        room.started = true;
        this.storeData.updateRoom(room.id, room);

        function sleep(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
        }

        while (!room.chessEngine.ready) {
            await sleep(200);
        }
        room.chessEngine.command(`position startpos moves${pastMoves}`);

        for (const chessJsMove of chessJsMoves) {
            console.log("ChessJs Fen", room.chessEngine.chessJs.fen());
            room.chessEngine.chessJs.move(chessJsMove);
        }


        while (!room.chessEngine.ready) {
            await sleep(200);
        }
        room.chessEngine.command(`d`);

    }

    getRoomByRoomId(roomid) {
        let room = this.storeData.getRoomByRoomId(roomid);
        return room ? Object.assign(new Room, room) : null;
    }

    addPlayerToRoom(player, roomid) {
        let room = this.getRoomByRoomId(roomid);
        let feedback = room.playerJoin(player);
        if (feedback === 1) {
            this.storeData.updateRoom(roomid, room);
            return room;
        } else {
            return null;
        }

    }

    removePlayerFromRoom(roomid, player) {
        let room = this.getRoomByRoomId(roomid);
        room.playerLeave(player);
        if (room.isEmpty()) {
            console.log(`room ${roomid} closed`);
            this.closeRoomByRoomId(roomid);
        } else {
            this.storeData.updateRoom(roomid, room);
        }
    }

    closeRoomByRoomId(roomid) {
        this.storeData.delRoomByRoomId(roomid);
    }
}

module.exports = RoomsService;