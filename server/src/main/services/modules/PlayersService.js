// TODO REVIEW

'use strict';

class PlayersService {

    constructor(storeData) {
        this._storeData = storeData;
    }

    get storeData() {
        return this._storeData;
    }

    playerJoinRoom(player, roomid) {
        player.room = roomid;
    }

    playerLeaveRoom(player) {
        player.room = -1;
    }

    getPlayerById(playerid) {
        let player = this.storeData.getPlayerById(playerid);
        return player;
    }


    getPlayerByClientId(clientid) {
        return this.storeData.getPlayerByClientId(clientid);
    }

    getOldClientIdByPlayerId(playerid) {
        return this.storeData.getClientIdByPlayerId(playerid);
    }

    registerNewPlayer(clientid, player) {
        this.storeData.registerPlayer(clientid, player);
    }

    unregisterPlayer(clientid) {
        this.storeData.unregisterPlayer(clientid);
    }

    /*
        Retire un joueur de la liste des connectés
     */
    playerDisconnected(clientid) {
        this.storeData.removePlayer(clientid);
    }

}

module.exports = PlayersService;