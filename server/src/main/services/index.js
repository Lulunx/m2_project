const {store} = require('../socket/store');
const StoreData = require('./StoreData');
const storeData = new StoreData(store);

const RoomsService = require('./modules/RoomsService');
const PlayersService = require('./modules/PlayersService');

module.exports = {
    roomsService: new RoomsService(storeData),
    playersService: new PlayersService(storeData)
};