// TODO REVIEW

'use strict';

class StoreData {

    constructor(store) {
        this._store = store;
    }

    get store() {
        return this._store;
    }

    /*
        PLAYERS
     */
    registerPlayer(clientid, player) {
        this.store.players.set(clientid, player);
    }

    getPlayerById(playerid) {
        let player = null;
        this.store.players.forEach((value, key) => {
            if (Number(value.playerid) === Number(playerid)) {
                player = value;
            }
        });
        return player;
    }

    getPlayerByClientId(clientid) {
        let player = this.store.players.get(clientid);
        return player ? player : null;
    }

    unregisterPlayer(clientid) {
        this.store.players.delete(clientid);
    }

    getClientIdByPlayerId(playerid) {
        this.store.players.forEach((value, key) => {
            if (value.playerid === playerid) {
                return key;
            }
        });
        return null;
    }

    getPlayers() {
        return this.store.players;
    }

    /*
        ROOMS
     */
    newRoom(room) {
        this.store.rooms.map.set(room.id, room)
    }

    updateRoom(roomid, room) {
        this.store.rooms.map.set(roomid, room);
    }

    delRoomByRoomId(roomid) {
        this.store.rooms.map.delete(roomid);
    }

    getNewRoomId() {
        let id = this.store.rooms.lastId + 1;
        this.store.rooms.lastId++;
        return id;
    }


    getRoomByRoomId(roomid) {
        let room = this.store.rooms.map.get(roomid);
        return room ? room : null;
    }

}

module.exports = StoreData;