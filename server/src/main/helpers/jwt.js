'use strict';
const expressJwt = require('express-jwt');
const Settings = require('../../../settings');
const UsersData = require('../database/UsersData');
const db = require('../database/conn');

const userData = new UsersData(db);

function jwt() {
    const secret = Settings.secret;
    return expressJwt({secret, isRevoked}).unless({
        path: [
            // public routes that don't require authentication
            '/users/authenticate',
            '/users/register',
        ]
    });
}

async function isRevoked(req, payload, done) {
    const user = await userData.getUser(payload.sub);

    // revoke token if user no longer exists
    if (!user) {
        return done(null, true);
    }

    done();
}

module.exports = jwt;

//https://github.com/cornflourblue/node-mongo-registration-login-api/blob/master/_helpers/jwt.js
